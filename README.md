Libhydro ![icon](https://bitbucket.org/pch_fr/libhydro/raw/stable/icon.jpg)
===============================================================================

Présentation
-------------------------------------------------------------------------------
Libhydro contient un ensemble de modules Python permettant de manipuler
les objets modélisés dans les dictionnaires Hydrométrie publiés par le SANDRE:

  * [Référentiel hydrométrique](http://www.sandre.eaufrance.fr/notice-doc/referentiel-hydrometrique-0)
  * [Processus d'accquisition des données hydrométriques](http://www.sandre.eaufrance.fr/notice-doc/processus-dacquisition-des-donnees-hydrometriques-0)

La libraire contient aussi plusieurs convertisseurs pour différents formats
de données hydrométriques.

Se reporter à la documentation et au tutoriel pour l'utilisation des différents modules.

Installation rapide sous Linux avec [Pip](https://pip.pypa.io/en/stable/)
-------------------------------------------------------------------------------
Libhydro fonctionne avec Python 2.7 uniquement et utilise Numpy, Pandas ainsi
que Lxml.etree pour le convertisseur xml.

Les dépendances sont installés automatiquement par Pip qui permet d'installer
la librarie directement depuis Bitbucket en indiquant un numéro de révision
ou tout autre identificateur standard utilisé par Mercurial.

Exemple pour installer la révision la plus récente de la branche stable:
```
pip install hg+http://bitbucket.org/pch_fr/libhydro@stable
```

Exemple pour installer la version (le tag) 0.6.0:
```
pip install hg+http://bitbucket.org/pch_fr/libhydro@0.6.0
```

On peux aussi depuis la section "Downloads" de bitbucket récupérer un package
d'installation ou bien un état du dépôt et l'installer localement.

Installation rapide sous Windows avec [Miniconda](https://conda.io/docs/) et [Pip](https://pip.pypa.io/en/stable/)
-------------------------------------------------------------------------------
Libhydro fonctionne avec Python 2.7 uniquement et utilise Numpy, Pandas ainsi
que Lxml.etree pour le convertisseur xml.

Les dépendances, nécessitant une compilation, sont à installer préalablement
avec Conda, par exemple à l'aide du fichier de requirements à copier
localement:
```
conda install --file requirements.txt
```

On peux ensuite installer la libhydro avec Pip, de la même façon que sous Linux.

Documentation
-------------------------------------------------------------------------------
Elle est disponible dans la section downloads.

Contact
-------------------------------------------------------------------------------
- Sébastien Romon <sebastien.romon@developpement-durable.gouv.fr>
- Philippe Gouin <philippe.gouin@developpement-durable.gouv.fr>
