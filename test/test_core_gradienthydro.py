# -*- coding: utf-8 -*-
"""Test program for gradienthydro.

To run all tests just type:
    python -m unittest test_core_gradienthydro

To run only a class test:
    python -m unittest test_core_gradienthydro.TestClass

To run only a specific test:
    python -m unittest test_core_gradienthydro.TestClass.test_method

"""
# -- imports ------------------------------------------------------------------
from __future__ import (
    unicode_literals as _unicode_literals,
    absolute_import as _absolute_import,
    division as _division,
    print_function as _print_function
)

import unittest
import datetime
import numpy as _numpy
from libhydro.core.gradienthydro import (Gradient, Gradients, SerieGradients)

from libhydro.core import (sitehydro as _sitehydro,
                           intervenant as _intervenant, _composant)

# -- strings ------------------------------------------------------------------
__author__ = """Sébastien ROMON""" \
             """<sebastien.romon@developpement-durable.gouv.fr>"""
__version__ = """0.1"""
__date__ = """2018-02-12"""

# HISTORY
# V0.1
# V0.1 - 2018-02-12
#   first shot


# -- class TestGradient --------------------------------------------
class TestGradient(unittest.TestCase):
    """"ObservationElaboree class tests."""

    def test_base_01(self):
        """Base case test."""
        dte = datetime.datetime(2016, 2, 10, 9, 17, 43)
        res = 100.5
        qal = 20
        mth = 8
        statut = 8
        obs = Gradient(dte=dte, res=res, mth=mth, qal=qal, statut=statut)
        self.assertEqual(obs.item(),
                         (dte, res, mth, qal, statut))

    def test_base_02(self):
        """Check default values."""
        # dte = datetime.datetime(2016, 2, 10, 9, 17, 43)
        obs = Gradient()
        statut = 0
        qal = 16
        mth = 0
        dte = None
        res = 0.0
        self.assertEqual(obs.item(),
                         (dte, res, mth, qal, statut))

    def test_base_03(self):
        """Check instanciatiosn."""
        dte = datetime.datetime(2016, 2, 10, 9, 17, 43)
        res = 100.5
        qal = 20
        mth = 8
        statut = 8
        obs = Gradient(dte=dte, res=res, mth=mth, qal=qal, statut=statut)
        self.assertEqual(obs.item(),
                         (dte, res, mth, qal, statut))
        obs = Gradient(dte, res, mth, qal, statut)
        self.assertEqual(obs.item(),
                         (dte, res, mth, qal, statut))
        dte2 = '2016-02-10T09:17:43'
        obs = Gradient(dte2, res, mth, qal, statut)
        self.assertEqual(obs.item(),
                         (dte, res, mth, qal, statut))
        dte3 = _numpy.datetime64(dte, 's')
        obs = Gradient(dte3, res, mth, qal, statut)
        self.assertEqual(obs.item(),
                         (dte, res, mth, qal, statut))

    def test_str_01(self):
        dte = datetime.datetime(2016, 2, 10, 9, 17, 43)
        res = 100.5
        qal = 20
        mth = 8
        statut = 8
        obs = Gradient(dte=dte, res=res, mth=mth, qal=qal, statut=statut)
        obs.__str__()
        # print(obs)

    def test_error_02(self):
        """dte error."""
        dte = datetime.datetime(2016, 2, 10, 9, 17, 43)
        res = 100.5
        qal = 20
        mth = 8
        statut = 8
        Gradient(dte=dte, res=res, mth=mth, qal=qal, statut=statut)
        dte = 'a'
        with self.assertRaises(ValueError) as cm:
            Gradient(dte=dte, res=res, mth=mth, qal=qal, statut=statut)

        dte = '2018-15-01 00:05:06'
        with self.assertRaises(ValueError) as cm:
            Gradient(dte=dte, res=res, mth=mth, qal=qal, statut=statut)

    def test_error_03(self):
        """res error."""
        dte = datetime.datetime(2016, 2, 10, 9, 17, 43)
        res = 100.5
        qal = 20
        mth = 8
        statut = 8
        Gradient(dte=dte, res=res, mth=mth, qal=qal, statut=statut)
        res = 'a'
        with self.assertRaises(ValueError) as cm:
            Gradient(dte=dte, res=res, mth=mth, qal=qal, statut=statut)

    def test_error_04(self):
        """qal error."""
        dte = datetime.datetime(2016, 2, 10, 9, 17, 43)
        res = 100.5
        qal = 20
        mth = 8
        statut = 8
        Gradient(dte=dte, res=res, mth=mth, qal=qal, statut=statut)
        for qal in [-5, 102, 'a']:
            with self.assertRaises(ValueError) as cm:
                Gradient(dte=dte, res=res, mth=mth, qal=qal, statut=statut)
            self.assertEqual(str(cm.exception),
                             'incorrect qualification')

    def test_error_05(self):
        """mth error."""
        dte = datetime.datetime(2016, 2, 10, 9, 17, 43)
        res = 100.5
        qal = 20
        mth = 8
        statut = 8
        Gradient(dte=dte, res=res, mth=mth, qal=qal, statut=statut)
        for mth in [-5, 102, 'a']:
            with self.assertRaises(ValueError) as cm:
                Gradient(dte=dte, res=res, mth=mth, qal=qal, statut=statut)
            self.assertEqual(str(cm.exception),
                             'incorrect method')

    def test_error_06(self):
        """statut error."""
        dte = datetime.datetime(2016, 2, 10, 9, 17, 43)
        res = 100.5
        qal = 20
        mth = 8
        statut = 8
        Gradient(dte=dte, res=res, mth=mth, qal=qal, statut=statut)
        for statut in [-5, 102, 'a']:
            with self.assertRaises(ValueError) as cm:
                Gradient(dte=dte, res=res, mth=mth, qal=qal, statut=statut)
            self.assertEqual(str(cm.exception),
                             'incorrect statut')


# -- class TestGradientshydro ------------------------------------------
class TestGradientshydro(unittest.TestCase):
    """Gradientshydro class tests."""

    def test_base_01(self):
        """Simple test."""
        # The simpliest __init_: datetime and res
        obs = Gradients(
            Gradient('2012-10-03 06:00', 33),
            Gradient('2012-10-03 07:00', 37),
            Gradient('2012-10-03 08:00', 42)
        )
        self.assertEqual(
            obs['res'].tolist(),
            [33, 37, 42]
        )

    def test_base_02(self):
        """Simple test."""
        # The simpliest __init_: datetime and res
        obs = Gradients(
            Gradient(res=33),
            Gradient('2012-10-03 07:00', 37),
            Gradient('2012-10-03 08:00', 42)
        )
        self.assertEqual(
            obs['res'].tolist(),
            [33, 37, 42]
        )

    def test_base_03(self):
        observations = []
        obs = Gradients(*observations)
        Gradients()
        # self.assertIsNone(obs)

    def test_error_01(self):
        """Error test"""
        with self.assertRaises(TypeError):
            Gradients(
                33,
                Gradient('2012-10-03 07:00', 37),
                Gradient('2012-10-03 08:00', 42)
                )


# -- class TestGradientshydroConcat ------------------------------------
class TestGradientshydroConcat(unittest.TestCase):
    """Test static method concat class ObservationsElaborees"""

    def test_base_01(self):
        obs1 = Gradients(
            Gradient('2012-10-03 06:00', 33),
            Gradient('2012-10-03 07:00', 37),
            Gradient('2012-10-03 08:00', 42)
        )
        obs2 = Gradients(
            Gradient('2012-10-04 06:00', 330),
            Gradient('2012-10-04 07:00', 370),
            Gradient('2012-10-04 08:00', 420)
        )

        obs = Gradients.concat(obs1, obs2)
        self.assertEqual(len(obs), 6)

    def test_base_02(self):
        obs1 = Gradients(
            Gradient('2012-10-03 06:00', 33),
            Gradient('2012-10-03 07:00', 37),
            Gradient('2012-10-03 08:00', 42)
        )
        obs2 = Gradient('2012-10-04 06:00', 330)
        obs = Gradients.concat(obs1, obs2)
        self.assertEqual(len(obs), 4)

# -- class TestSerieGradients ------------------------------------------
class TestSerieGradients(unittest.TestCase):
    """"SerieGradients class tests."""

    def test_base_01(self):
        """Simple test"""
        entite = _sitehydro.Sitehydro(code='A1234567')
        dtprod = datetime.datetime(2015, 3, 4, 15, 47, 23)
        contact = _intervenant.Contact(code='154')
        grd = 'H'
        duree = 60
        gradients = Gradients(
            Gradient(res=33),
            Gradient('2012-10-03 07:00', 37),
            Gradient('2012-10-03 08:00', 42)
        )
        serie = SerieGradients(entite=entite, grd=grd, duree=duree,
                               contact=contact, gradients=gradients,
                               dtprod=dtprod)
        self.assertEqual((serie.entite, serie.grd, serie.duree,
                          serie.gradients, serie.contact, serie.dtprod),
                         (entite, grd, duree, gradients, contact, dtprod))

    def test_base_02(self):
        """default value"""
        entite = _sitehydro.Station(code='A123456789')
        dtprod = datetime.datetime(2015, 3, 4, 15, 47, 23)
        grd = 'Q'
        duree = 15
        serie = SerieGradients(entite=entite, dtprod=dtprod, grd=grd,
                               duree=duree)
        self.assertEqual((serie.entite, serie.dtprod, serie.grd, serie.duree,
                          serie.contact, serie.gradients),
                         (entite, dtprod, grd, duree,
                          None, None))

    def test_base_03(self):
        """different values of typegrd and pdt"""
        entite = _sitehydro.Station(code='A123456789')
        dtprod = datetime.datetime(2015, 3, 4, 15, 47, 23)
        grd = 'H'
        duree = 5
        serie = SerieGradients(entite=entite, dtprod=dtprod, grd=grd,
                                   duree=duree)
        self.assertEqual((serie.entite, serie.dtprod, serie.grd, serie.duree,
                          serie.gradients, serie.contact),
                         (entite, dtprod, grd, duree,
                          None, None))

        entite = _sitehydro.Capteur(code='A12345678901')
        dtprod = datetime.datetime(2015, 3, 4, 15, 47, 23)
        grd = 'Q'
        duree = 120
        serie = SerieGradients(entite=entite, dtprod=dtprod, grd=grd,
                               duree=duree)
        self.assertEqual((serie.entite, serie.dtprod, serie.grd, serie.duree,
                          serie.contact),
                         (entite, dtprod, grd, duree, None))

    def test_str_01(self):
        """Serie representation test"""
        entite = _sitehydro.Station(code='A123456789')
        dtprod = datetime.datetime(2015, 3, 4, 15, 47, 23)
        grd = 'H'
        duree = 30
        serie = SerieGradients(entite=entite, dtprod=dtprod, grd=grd,
                               duree=duree)
        self.assertTrue(serie.__unicode__().find('<sans gradients>') != -1)

    def test_str_02(self):
        """Serie representation with observations test"""
        entite = _sitehydro.Station(code='A123456789')
        dtprod = datetime.datetime(2015, 3, 4, 15, 47, 23)
        grd = 'Q'
        duree = 60
        gradients = Gradients(
            Gradient(res=33),
            Gradient('2012-10-03 07:00', 37),
            Gradient('2012-10-03 08:00', 42)
        )
        serie = SerieGradients(entite=entite, dtprod=dtprod, grd=grd,
                               duree=duree, gradients=gradients)
        self.assertTrue(serie.__unicode__().find(grd) != -1)
        self.assertTrue(serie.__unicode__().find('37') != -1)
        self.assertTrue(serie.__unicode__().find('2012-10-03 08:00:00') != -1)
        # print(serie)

    def test_error_entite(self):
        """entite errorr"""
        entite = _sitehydro.Station(code='A123456789')
        dtprod = datetime.datetime(2015, 3, 4, 15, 47, 23)
        grd = 'H'
        duree = 60
        for entite in [_sitehydro.Station(code='A123456789'),
                       _sitehydro.Sitehydro(code='A1234567'),
                       _sitehydro.Capteur(code='A12345678901')]:
            SerieGradients(entite=entite, dtprod=dtprod, grd=grd, duree=duree)

        for entite in [None, 'A1234567']:
            with self.assertRaises(TypeError) as cm:
                SerieGradients(entite=entite, dtprod=dtprod, grd=grd,
                               duree=duree)
            # print(cm.exception)

    def test_error_dtprod(self):
        """dtprod error."""
        entite = _sitehydro.Station(code='A123456789')
        dtprod = datetime.datetime(2015, 3, 4, 15, 47, 23)
        grd = 'Q'
        duree = 15
        SerieGradients(entite=entite, dtprod=dtprod, grd=grd, duree=duree)

        for dtprod in [None, 'a']:
            with self.assertRaises(Exception) as cm:
                SerieGradients(entite=entite, dtprod=dtprod, grd=grd,
                               duree=duree)
            # print(cm.exception)

    def test_error_grd(self):
        """typegrd error."""
        entite = _sitehydro.Station(code='A123456789')
        dtprod = datetime.datetime(2015, 3, 4, 15, 47, 23)
        duree = 60
        for grd in ['H', 'Q']:
            SerieGradients(entite=entite, dtprod=dtprod, grd=grd, duree=duree)

        for grd in [None, 'HQ']:
            with self.assertRaises(Exception) as cm:
                SerieGradients(entite=entite, dtprod=dtprod, grd=grd,
                               duree=duree)
            # print(cm.exception)

    def test_error_duree(self):
        """test error pdt in hours"""
        entite = _sitehydro.Station(code='A123456789')
        dtprod = datetime.datetime(2015, 3, 4, 15, 47, 23)
        grd = 'H'
        duree = 20
        SerieGradients(entite=entite, dtprod=dtprod, grd=grd, duree=duree)
        for duree in [None, -5, 0, 'toto']:
            with self.assertRaises(Exception) as cm:
                SerieGradients(entite=entite, dtprod=dtprod, grd=grd,
                               duree=duree)

    def test_error_contact(self):
        entite = _sitehydro.Station(code='A123456789')
        dtprod = datetime.datetime(2015, 3, 4, 15, 47, 23)
        grd = 'Q'
        duree = 60
        contact = _intervenant.Contact(code='1234')
        SerieGradients(entite=entite, dtprod=dtprod, grd=grd, duree=duree,
                       contact=contact)
        for contact in ['123', 'toto']:
            with self.assertRaises(Exception):
                SerieGradients(entite=entite, dtprod=dtprod, grd=grd,
                               duree=duree, contact=contact)

    def test_error_gradients(self):
        entite = _sitehydro.Station(code='A123456789')
        dtprod = datetime.datetime(2015, 3, 4, 15, 47, 23)
        grd = 'Q'
        duree = 60
        gradients = Gradients(
            Gradient(res=33),
            Gradient('2012-10-03 07:00', 37),
            Gradient('2012-10-03 08:00', 42)
        )
        SerieGradients(entite=entite, dtprod=dtprod, grd=grd, duree=duree,
                       gradients=gradients)
        for gradients in [[45, 18], 38]:
            with self.assertRaises(Exception):
                SerieGradients(entite=entite, dtprod=dtprod, grd=grd,
                               duree=duree, gradients=gradients)
